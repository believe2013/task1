<?php
require_once __DIR__ . '/autoload.php';

use Applications\Classes\searchFlight as searchFlight;

// run app
try {
    $searchFlight = new searchFlight();
    $searchFlight->send_and_save();
} catch (Exception $e) {
    echo $e->getMessage();
    die();
}

