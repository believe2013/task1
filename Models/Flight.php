<?php
namespace Applications\Models;
use Applications\Classes\AbstractModel as AbstractModel;
/**
 * Class Flight
 * @property $id
 * @property $from
 * @property $to
 * @property $back
 * @property $start
 * @property $stop
 * @property $adult
 * @property $child
 * @property $infant
 * @property $price
 */
class Flight
    extends AbstractModel
{
    protected static $table = "flights";
}