<?php
namespace Applications\Models;
use Applications\Classes\AbstractModel as AbstractModel;
/**
 * Class Airport
 * @property $id
 * @property $code
 * @property $name
 * @property $contry
 */
class Airport
    extends AbstractModel
{
    protected static $table = "airports";
}