<?php
namespace Applications\Classes;

//use Applications\Classes\MigrationDemo;
use Applications\Models\Airport;
use Applications\Models\Flight;

class searchFlight
{
    //public static $debug = true;

    /**
     * @method query_xml
     * @param $url
     * @return mixed
     */
    private function query_xml($url)
    {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * @method parce_xml
     * @param $data
     * @return array
     */
    private function parce_xml($data)
    {
        // xml строку в объект
        $sxml = simplexml_load_string($data);

        if($sxml->Result[0] == 'SUCCESS'){
            $airport = new Airport();
            $arTmpFlights = [];
            $i = 0;
            foreach ($sxml->ShopOptions as $shop_option){
                $j = 0;
                $arTmpFlights[$i] = [];
                $start = '';
                $stop = '';

                // рейс
                foreach ($shop_option->ShopOption->ItineraryOptions->ItineraryOption as $item){
                    if(!$airport::findOneByColumn('code', $item['From']) OR !$airport::findOneByColumn('code', $item['To']))
                        exit('Неизвестный аэропорт в записи');

                    $arTmpFlights[$i][$j]["FROM"] = (string)$item['From'];
                    $arTmpFlights[$i][$j]["TO"] = (string)$item['To'];

                    foreach ($item->FlightSegment as $it){
                        if($item['ODRef'] == '01') {
                            if($it->Departure['Time'])
                                $start = $it->Departure['Time'];
                            $arTmpFlights[$i][$j]["BACK"] = 0;
                        } else {
                            if($it->Arrival['Time'])
                                $stop = $it->Arrival['Time'];
                            $arTmpFlights[$i][$j]["BACK"] = 1;
                        }
                    }


                    $j++;
                }
                $k = 0;
                foreach ($shop_option->ShopOption->ItineraryOptions->ItineraryOption as $item){
                    foreach ($item->FlightSegment as $it){
                        $arTmpFlights[$i][$k]["START"] = (string)$start;
                        $arTmpFlights[$i][$k]["STOP"] = (string)$stop;
                    }
                    $k++;
                }

                // пассажиры
                $j=0;
                $price = 0.00;
                $adult = 0;
                $child = 0;
                $infant = 0;
                foreach ($shop_option->ShopOption->FareInfo->Fares->Fare as $fare){
                    $price += $fare->Price['Total'];
                    switch ($fare->PaxType['AgeCat']) {
                        case 'ADT': $adult = (int)$fare->PaxType['Count']; break;
                        case 'CLD': $child = (int)$fare->PaxType['Count']; break;
                        case 'INF': $infant = (int)$fare->PaxType['Count']; break;
                    }
                    $j++;
                }
                $j=0;
                foreach ($shop_option->ShopOption->FareInfo->Fares->Fare as $fare){
                    $arTmpFlights[$i][$j]['PRICE'] = number_format((int)$price, 2, '.', '');
                    $arTmpFlights[$i][$j]['ADULT'] = $adult;
                    $arTmpFlights[$i][$j]['CHILD'] = $child;
                    $arTmpFlights[$i][$j]['INFANT'] = $infant;
                    $j++;
                }

                $i++;
            }
            $items = [];
            $i = 0;
            foreach ($arTmpFlights as $its){
                foreach ($its as $item){
                    $items[] = $item;
                    $i++;
                }
            }
            // форматируем дату
            foreach ($items as &$item){
                $item['START'] = date('Y-m-d', strtotime($item['START']));
                $item['STOP'] = date('Y-m-d', strtotime($item['START']));
            }

            return $items;
        }
    }


    /**
     * @method save_to_DB
     * @param $items
     */
    public function save_to_DB($items)
    {
        $migrate = new MigrationDemo();
        $migrate->execute();

        foreach ($items as $item){
            $flight = new Flight();
            foreach ($item as $k => $prop){
                $k_low = strtolower($k);
                $flight->{$k_low} = $prop;
            }
            $flight->insert();
            $flight->save();
        }

    }

    /**
     * @method send_and_save
     */
    public function send_and_save()
    {
        $xml_data = self::query_xml('http://task.ru/server/index.php');

        $items = $this->parce_xml($xml_data);
        $this->save_to_DB($items);
    }
}