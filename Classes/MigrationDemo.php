<?php

namespace Applications\Classes;

class MigrationDemo
{
    private $sql;

    function __construct()
    {
        $this->sql = "CREATE TABLE IF NOT EXISTS flights(
                    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, 
                    `from` SMALLINT (6) UNSIGNED DEFAULT 0,
                    `to` SMALLINT (6) UNSIGNED DEFAULT 0,
                    back TINYINT (1) UNSIGNED DEFAULT 0,
                    start DATE DEFAULT NULL,
                    stop DATE DEFAULT NULL,
                    adult TINYINT (1) UNSIGNED DEFAULT 0,
                    child TINYINT (1) UNSIGNED DEFAULT 0,
                    infant TINYINT (1) UNSIGNED DEFAULT 0,
                    price DECIMAL (12,2) DEFAULT 0.00,
                    PRIMARY KEY (id)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

        $this->sql .= "CREATE TABLE IF NOT EXISTS airports(
                    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, 
                    code VARCHAR (3) DEFAULT '',
                    `name` VARCHAR (64) DEFAULT '',
                    country SMALLINT (6) UNSIGNED DEFAULT 0,
                    PRIMARY KEY (id)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
    }
    public function execute(){
        $db = new DB();
        $db->execute($this->sql);
    }
}