<?php
namespace Applications\Classes;
class DB
{
    private $dbh;
    private $className = 'stdClass';
    public function __construct()
    {
        try {
            $this->dbh = new \PDO('mysql:host=127.0.0.1;dbname=task', 'root', '');
            $this->dbh->exec("SET NAMES utf8");
        }
        catch (\PDOException $edb) {
            echo 'Связь с БД не установлена';
        }
    }
    public function setClassName($className)
    {
        $this->className = $className;
    }
    /**
     * @param $sql
     * @param array $params
     * @return array
     */
    public function query($sql, $params=[])
    {
        $sth = $this->dbh->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll(\PDO::FETCH_CLASS, $this->className);
    }
    public function execute($sql, $params=[])
    {
        $sth = $this->dbh->prepare($sql);
        return $sth->execute($params);
    }
    public function lastInsertId_0()
    {
        return $this->dbh->lastInsertId();
    }
}