<?php

namespace Applications\Classes;

abstract class AbstractModel
{
    static protected $table;
    protected $data = [];
    public function __set($k, $v)
    {
        $this->data[$k] = $v;
    }
    public function __get($k)
    {
        return $this->data[$k];
    }
    public function __isset($k)
    {
        return isset($this->data[$k]);
    }

    public static function findOneByColumn($column, $value) //findOneByColumn
    {
        $db = new DB();
        $db->setClassName(get_called_class());
        $sql = 'SELECT * FROM ' . static::$table . ' WHERE ' . $column . '=:value LIMIT 1';
        $res = $db->query($sql, [':value' => $value]);
        if (!empty($res)) {
            return $res[0];
        }
        return false;
    }
    
    public  function insert()
    {
        $db = new DB();
        $cols = array_keys($this->data);
        $data = [];
        foreach ($cols as $col) {
            $data[':' . $col] = $this->data[$col];
        }
        foreach ($cols as &$col){
            $col = '`'.$col.'`';
        }
        $sql = '
            INSERT INTO ' . static::$table . '
             (' . implode(', ', $cols). ')
             VALUES
             (' . implode(', ', array_keys($data)). ');
         ';
        $db->execute($sql, $data);
        $this->id_0 = $db->lastInsertId_0();
    }
    public function save()
    {
        if (!isset($this->id_0)) {
            $this->insert();
        }
    }
}